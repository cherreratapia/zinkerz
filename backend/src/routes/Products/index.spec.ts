import { productsHandlers } from "./index";
import { PRODUCTS_SKU } from "../../util/index";

describe("Product Router", () => {
  describe("Get by Part Number", () => {
    it("Should return the products details", async () => {
      const req: any = {
        params: {
          id: "1234"
        }
      };
      const res: any = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn()
      };
      const axios = {
        get: jest.fn().mockResolvedValue({
          data: { name: "Object 1", price: 1000 }
        })
      };
      const client = {
        get: jest.fn().mockImplementation((err, callback) => callback("test")),
        setex: jest.fn()
      };
      await productsHandlers({ axios, client }).getByParamNumber(req, res);
      expect(res.status.mock.calls).toEqual([[200]]);
      expect(res.json.mock.calls).toEqual([
        [{ data: { name: "Object 1", price: 1000 } }]
      ]);
    });
    it("Should return a 400 because it should fail the get", async () => {
      const req: any = {
        params: {
          id: "1234"
        }
      };
      const res: any = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn()
      };
      const axios = {
        get: jest.fn().mockImplementation(() => {
          throw new Error("Error de test");
        })
      };
      const client = {
        get: jest.fn().mockImplementation((err, callback) => callback("test")),
        setex: jest.fn()
      };

      await productsHandlers({ axios, client }).getByParamNumber(req, res);
      expect(res.status.mock.calls).toEqual([[400]]);
      expect(res.json.mock.calls).toEqual([
        [{ error: "Error al obtener el detalle del producto" }]
      ]);
    });
    it("Should return a 400 because the request no get the param", async () => {
      const req: any = {
        params: {}
      };
      const res: any = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn()
      };
      const axios = {
        get: jest.fn().mockImplementation()
      };
      const client = {
        get: jest.fn().mockImplementation((err, callback) => callback("test")),
        setex: jest.fn()
      };

      await productsHandlers({ axios, client }).getByParamNumber(req, res);
      expect(res.status.mock.calls).toEqual([[400]]);
      expect(res.json.mock.calls).toEqual([
        [{ error: "Error al obtener el detalle del producto" }]
      ]);
    });
  });
  describe("Get all products", () => {
    it("Should return all products", async () => {
      const req: any = jest.fn();
      const res: any = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn()
      };
      const axios = {
        get: jest.fn().mockResolvedValue({
          data: { name: "Object 1", price: 1000 }
        })
      };
      const client = {
        get: jest.fn().mockImplementation((err, callback) => callback("test")),
        setex: jest.fn()
      };
      await productsHandlers({ axios, client }).get(req, res);
      expect(res.status.mock.calls).toEqual([[200]]);
      expect(res.json.mock.calls).toEqual([
        [
          {
            data: [
              { name: "Object 1", price: 1000 },
              { name: "Object 1", price: 1000 },
              { name: "Object 1", price: 1000 },
              { name: "Object 1", price: 1000 },
              { name: "Object 1", price: 1000 },
              { name: "Object 1", price: 1000 },
              { name: "Object 1", price: 1000 },
              { name: "Object 1", price: 1000 },
              { name: "Object 1", price: 1000 },
              { name: "Object 1", price: 1000 },
              { name: "Object 1", price: 1000 },
              { name: "Object 1", price: 1000 },
              { name: "Object 1", price: 1000 },
              { name: "Object 1", price: 1000 },
              { name: "Object 1", price: 1000 },
              { name: "Object 1", price: 1000 },
              { name: "Object 1", price: 1000 },
              { name: "Object 1", price: 1000 },
              { name: "Object 1", price: 1000 },
              { name: "Object 1", price: 1000 },
              { name: "Object 1", price: 1000 },
              { name: "Object 1", price: 1000 }
            ]
          }
        ]
      ]);
    });
    it("Should return a 400 status because a SKU is wrong", async () => {
      const req: any = jest.fn();
      const res: any = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn()
      };
      const axios = {
        get: jest.fn().mockImplementation(() => {
          throw new Error("Error al obtener productos desde REDIS");
        })
      };
      const client = {
        get: jest.fn().mockImplementation((err, callback) => callback("test")),
        setex: jest.fn()
      };
      const SKUS = ["999"];
      await productsHandlers({ axios, client }, SKUS).get(req, res);
      expect(res.status.mock.calls).toEqual([[400]]);
      expect(res.json.mock.calls).toEqual([
        [{ error: "Error al obtener productos desde REDIS" }]
      ]);
    });
  });
});
