import cookieParser from "cookie-parser";
import express from "express";
import { Request, Response } from "express";
import logger from "morgan";
import path from "path";
import BaseRouter from "./routes";
import redis, { RedisError, RedisClient } from "redis";

import admin from "firebase-admin";
import axios from "axios";
import cors from "cors";
import { logger as LoggerPersistent } from "./shared/Logger";

const serviceAccount = require("../config/simple-ripley-9c988-firebase-adminsdk-mcxxq-a309bc538f.json");
// Init express
const app = express();

// Create client of redis
const client: RedisClient = redis.createClient({ host: "redis" });

// Add middleware/settings/routes to express.
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(cors());
app.use(express.static(path.join(__dirname, "public")));
app.use("/api/v1", BaseRouter({ axios, client }));

client.on("error", (err: RedisError) => {
  console.log("error en redis", err.message);
  LoggerPersistent.error(`Error on redis client: ${err.message}`);
});

// Export express instance
export default app;
