import config from "../config";
import axios from "axios-observable";
import { retryWhen, delay, tap, mergeMap } from "rxjs/operators";
import { Observable, of, throwError } from "rxjs";

const { baseURL, port, api, version } = config;

const get = () => {
  return axios.get(`${baseURL}:${port}/${api}/${version}/product`).pipe(
    retryWhen((errors: Observable<any>) => {
      return errors.pipe(
        delay(1500),
        mergeMap(error => {
          if (error.status && error.status === 403) {
            console.log("error en la consulta");
          } else {
            console.log("error en la consulta else");
          }
          return error.status === 403 ? throwError(error) : of(error);
        })
      );
    })
  );
};

const getByPartNumber = (partNumber: string) => {
  return axios
    .get(`${baseURL}:${port}/${api}/${version}/product/${partNumber}`)
    .pipe(
      retryWhen((errors: Observable<any>) => {
        return errors.pipe(
          delay(1500),
          mergeMap(error => {
            if (error.status && error.status === 403) {
              console.log(error);
            } else {
            }
            return error.status === 403 ? throwError(error) : of(error);
          })
        );
      })
    );
};

export { get, getByPartNumber };
