const dev = {
  baseURL: "http://localhost",
  port: 3000,
  api: "api",
  version: "v1"
};

const prod = {
  baseURL: "http://localhost",
  port: 3000,
  api: "api",
  version: "v1"
};

const config = process.env.PROD === "prod" ? prod : dev;

export default {
  ...config
};
