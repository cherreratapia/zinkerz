import { AvailableList } from "./available-list";

export interface Simple {
  lists: AvailableList;
  isUnavailable: boolean;
  isOutOfStock: boolean;
  isMarketplaceProduct: boolean;
}
