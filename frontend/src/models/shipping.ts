export interface Shipping {
  rTienda: boolean;
  dDomicilio: boolean;
  rCercano: boolean;
  cashOnDelivery: boolean;
}
