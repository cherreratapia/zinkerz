export interface Attribute {
  displayable: boolean;
  id: number;
  identifier: string;
  name: string;
  usage: string;
  value: string;
}
