export interface SkuPrice {
  SKUPriceDescription: string;
  SKUPriceValue: number;
  SKUPriceUsage: string;
}
