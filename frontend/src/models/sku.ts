import { SkuPrice } from "./sku-price";
import { Attribute } from "./attribute";

export interface SKU {
  Price: SkuPrice;
  SKUUniqueID: number;
  partNumber: string;
  xCatEntryQuantity: number;
  ineligible: boolean;
  Attributes: Attribute[];
  isMainProduct: boolean;
}
