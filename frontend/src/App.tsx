import React from "react";
import "./assets/css/normalize.css";
import "./assets/css/index.css";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "../node_modules/spinkit/spinkit.min.css";
import { Router, RouteComponentProps } from "@reach/router";
import ProductDetail from "./components/product-detail";
import ProductList from "./components/product-list";

const App: React.FC = () => {
  return (
    <Router>
      <ProductList path="/" />
      <ProductDetail path="/:partNumber" />
    </Router>
  );
};
export default App;
