import React, { useState, useEffect } from "react";
import { RouteComponentProps, navigate } from "@reach/router";
import { Button, Container, Box } from "@material-ui/core";
import { getByPartNumber } from "../services/product-service";
import { ProductModel } from "../models/product";

const ProductDetail: React.FC<RouteComponentProps> = (props: any) => {
  const [partNumber, setPartNumber] = useState(null);
  const [product, setProduct] = useState<ProductModel | null>(null);
  useEffect(() => {
    setPartNumber(props.partNumber);
    getByPartNumber(props.partNumber).subscribe(
      response => setProduct(response.data),
      error => {
        console.log("error ! ", error);
      }
    );
  }, []);

  const goBack = () => {
    navigate(`/`);
  };

  return (
    <div>
      {!product ? (
        <div className="w-full h-vh d-flex center">
          <div className="mb-2">Cargando el detalle del producto...</div>
          <div className="sk-chase">
            <div className="sk-chase-dot"></div>
            <div className="sk-chase-dot"></div>
            <div className="sk-chase-dot"></div>
            <div className="sk-chase-dot"></div>
            <div className="sk-chase-dot"></div>
            <div className="sk-chase-dot"></div>
          </div>
        </div>
      ) : (
        <Container>
          <Box display="flex" flexDirection="row">
            <Box className="col-7 product-detail_img-viewer">
              <img src={product.fullImage} alt={product.partNumber} />
            </Box>
            <Box display="flex" flexDirection="column" className="col-5">
              <Box display="flex" flexDirection="flex-row" alignItems="center">
                <h1 className="product-detail_name text-uppercase">
                  {product.name}
                </h1>
              </Box>
              <Box display="flex" flexDirection="row" alignItems="center">
                <p className="product-detail_sku">SKU: {product.partNumber}</p>
              </Box>
              <Box display="flex" flexDirection="row" alignItems="center">
                <p className="product-detail_short-description">
                  {product.shortDescription}
                </p>
              </Box>
              <Box display="flex" flexDirection="column">
                <Box
                  display="flex"
                  flexDirection="row"
                  alignItems="center"
                  className="mb-1"
                >
                  <Box
                    display="flex"
                    justifyContent="start"
                    className="p-0 col-6"
                  >
                    <p className="product-detail_price">Normal</p>
                  </Box>
                  <Box className="p-0 col-6 d-flex justify-content-end">
                    <p className="product-detail_price">
                      {product.prices.listPrice}
                    </p>
                  </Box>
                </Box>

                {product.prices.offerPrice && (
                  <Box
                    display="flex"
                    flexDirection="row"
                    alignItems="center"
                    className="mb-1"
                  >
                    <Box
                      display="flex"
                      justifyContent="start"
                      className="p-0 col-6"
                    >
                      {/*tslint-disable-next-line*/}
                      <p
                        className={
                          product.prices.cardPrice
                            ? "product-detail_price"
                            : "product-detail_price_lowest"
                        }
                      >
                        Oferta
                      </p>
                    </Box>
                    <Box className="p-0 col-6 d-flex justify-content-end">
                      {/*tslint-disable-next-line*/}
                      <p
                        className={
                          product.prices.cardPrice
                            ? "product-detail_price"
                            : "product-detail_price_lowest"
                        }
                      >
                        {product.prices.offerPrice}
                      </p>
                    </Box>
                  </Box>
                )}
                {product && product.prices && product.prices.cardPrice && (
                  <Box
                    display="flex"
                    flexDirection="row"
                    alignItems="center"
                    className="mb-1"
                  >
                    <Box
                      display="flex"
                      justifyContent="start"
                      className="p-0 col-6"
                    >
                      <p className="product-detail_price_lowest">
                        Tarjeta Ripley
                      </p>
                    </Box>
                    <Box className="p-0 col-6 d-flex justify-content-end">
                      <p className="product-detail_price_lowest">
                        {product.prices.cardPrice}
                      </p>
                    </Box>
                  </Box>
                )}

                {product &&
                  product.prices &&
                  product.prices.discountPercentage && (
                    <Box
                      display="flex"
                      flexDirection="row"
                      alignItems="center"
                      className="mb-1"
                    >
                      <Box
                        display="flex"
                        justifyContent="start"
                        className="p-0 col-6"
                      >
                        <p className="product-detail_discount">Descuento</p>
                      </Box>

                      <Box className="p-0 col-6 d-flex justify-content-end">
                        <Box className="product-detail_discount_arrow">
                          <p className="product-detail_discount_arrow_text">
                            {product.prices.discountPercentage} %
                          </p>
                        </Box>
                      </Box>
                    </Box>
                  )}
                {product && product.prices && product.prices.ripleyPuntos && (
                  <Box
                    display="flex"
                    flexDirection="row"
                    alignItems="center"
                    className="mb-1 product-detail_points"
                  >
                    <Box
                      display="flex"
                      justifyContent="start"
                      className="p-0 col-6"
                    >
                      <p className="product-detail_points_text">Acumulas</p>
                    </Box>

                    <Box className="p-0 col-6 d-flex justify-content-end">
                      <p className="product-detail_points_text">
                        {product.prices.ripleyPuntos} RipleyPuntos GO
                      </p>
                    </Box>
                  </Box>
                )}

                <Box display="flex" justifyContent="end" flexDirection="row">
                  <Button
                    color="primary"
                    className="w-100"
                    variant="contained"
                    onClick={event => goBack()}
                  >
                    Volver
                  </Button>
                </Box>
              </Box>
            </Box>
          </Box>
        </Container>
      )}
    </div>
  );
};

export default ProductDetail;
