import React, { useEffect, useState } from "react";
import { RouteComponentProps, Link, navigate } from "@reach/router";
import { Button, Container, Grid, Box } from "@material-ui/core";
import NumberFormat from "react-number-format";
import { CreditCardRounded } from "@material-ui/icons";
import { get } from "../services/product-service";
import { ProductModel } from "../models/product";

const ProductList: React.FC<RouteComponentProps> = () => {
  const [productList, setProductList] = useState<ProductModel[] | null>(null);
  useEffect(() => {
    get().subscribe(
      resp => {
        setProductList([...resp.data]);
      },
      err => {
        console.log(`error desde el componente ${err}`);
      }
    );
  }, []);

  const goToDetail = (partNumber: string) => {
    navigate(`${partNumber}`);
  };

  return !productList ? (
    <div className="w-full h-vh d-flex center">
      <div className="mb-2">Cargando el detalle del producto...</div>
      <div className="sk-chase">
        <div className="sk-chase-dot"></div>
        <div className="sk-chase-dot"></div>
        <div className="sk-chase-dot"></div>
        <div className="sk-chase-dot"></div>
        <div className="sk-chase-dot"></div>
        <div className="sk-chase-dot"></div>
      </div>
    </div>
  ) : (
    <Container>
      <Grid container spacing={1}>
        {productList &&
          productList.map((product: ProductModel) => (
            <Grid
              key={product.partNumber}
              item
              xs={4}
              className="cursor-pointer mb-item"
            >
              <Box display="flex" flexDirection="column">
                <div onClick={() => goToDetail(product.partNumber)}>
                  <Box
                    className="position-relative"
                    display="flex"
                    flexDirection="row"
                    justifyContent="flex-end"
                    flex="1"
                  >
                    <img className="img-fluid" src={product.fullImage} />
                    {product.prices.discount && (
                      <Box
                        display="flex"
                        flexDirection="row"
                        justifyContent="center"
                        alignItems="center"
                        className="position-absolute discount"
                      >
                        <p>- {product.prices.discountPercentage} %</p>
                      </Box>
                    )}
                  </Box>
                  <Box display="flex" flexDirection="column" className="mb-2">
                    <p className="text-weight-bold text-uppercase brand">
                      {product.attributes[0].value}
                    </p>
                  </Box>
                  <Box display="flex" className="mb-2">
                    <p className="name">{product.name}</p>
                  </Box>
                  <Box display="flex" flexDirection="column">
                    <p
                      className={
                        product.prices.offerPrice && product.prices.cardPrice
                          ? "lowest-price"
                          : "list-price"
                      }
                    >
                      {
                        <NumberFormat
                          value={product.prices.listPrice}
                          displayType={"text"}
                          thousandSeparator={"."}
                          decimalSeparator={","}
                          prefix={"$"}
                        />
                      }
                    </p>
                    <p
                      className={
                        product.prices.cardPrice
                          ? "offer-price"
                          : "lowest-price"
                      }
                    >
                      {
                        <NumberFormat
                          value={product.prices.offerPrice}
                          displayType={"text"}
                          thousandSeparator={"."}
                          decimalSeparator={","}
                          prefix={"$"}
                        />
                      }
                    </p>
                    {product.prices.cardPrice && (
                      <p className="lowest-price">
                        <CreditCardRounded />
                        <NumberFormat
                          value={product.prices.cardPrice}
                          displayType={"text"}
                          thousandSeparator={"."}
                          decimalSeparator={","}
                          prefix={"$"}
                        />
                      </p>
                    )}
                  </Box>
                </div>
              </Box>
            </Grid>
          ))}
      </Grid>
    </Container>
  );
};
export default ProductList;
